from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView, LogoutView, login_required
from django.views.generic import CreateView, UpdateView
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy
from .forms import RegistrationForm, UpdateForm

from .models import User, City

# Create your views here.
class Login(LoginView):
    template_name = 'users/login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = 'Iniciar sesión'
        return context
class Register(CreateView):
    form_class = RegistrationForm
    template_name = 'users/register.html'
    success_url = reverse_lazy('posts:feed')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('posts:feed')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = 'Crear cuenta'
        return context

    def form_valid(self, form):
        valid = super(Register, self).form_valid(form)
        username, password = form.cleaned_data.get('username'), form.cleaned_data.get('password1')
        new_user = authenticate(self.request, username=username, password=password)
        login(self.request, new_user)
        return valid

class UserUpdateProfile(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'users/update.html'
    form_class = UpdateForm

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        return reverse_lazy('users:profile', kwargs={'username': self.request.user.username })

# AJAX
def load_cities(request):
    province_id = request.GET.get('province')
    cities = City.objects.filter(province=province_id).order_by('name')
    return render(request, 'users/cities_list_options.html', { 'cities': cities })

@login_required
def UserProfile(request, username):
    user = User.objects.get(username=username)
    return render(request, 'users/profile.html', { 'profile': user })