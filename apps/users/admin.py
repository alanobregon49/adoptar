from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(User)
admin.site.register(Province)
admin.site.register(City)
admin.site.register(Reason)
admin.site.register(Complaint)
admin.site.register(Qualification)