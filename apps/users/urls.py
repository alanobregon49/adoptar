from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

from django.urls import reverse_lazy

app_name = 'users'
urlpatterns = [
    path('login/', views.Login.as_view(redirect_authenticated_user=True), name="login"),
    path('logout/', views.LogoutView.as_view(), name="logout"),
    path('register/', views.Register.as_view(), name="register"),

    path('password_change/', auth_views.PasswordChangeView.as_view(
        template_name="users/password_change/change.html",success_url=reverse_lazy('users:password_change_done')), name="password_change"),

    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(
        template_name="users/password_change/done.html"), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(
        template_name='users/password_reset/reset.html', 
        email_template_name='users/password_reset/email.html', 
        subject_template_name="users/password_reset/subject.txt",
        success_url=reverse_lazy('users:password_reset_done')), name="password_reset"),

    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(
        template_name='users/password_reset/done.html'), name="password_reset_done"),

    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='users/password_reset/confirm.html',
        success_url=reverse_lazy('users:password_reset_complete')), name="password_reset_confirm"),

    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(
        template_name='users/password_reset/complete.html'), name="password_reset_complete"),

    path("update/", views.UserUpdateProfile.as_view(), name="update_profile"),
    path('<str:username>/', views.UserProfile, name="profile"),

    path("ajax/load-cities", views.load_cities, name="ajax_load_cities"),
]
