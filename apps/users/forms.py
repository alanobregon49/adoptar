from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import User, City

class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'province',
            'password1',
            'password2'
        ]

        help_texts = {
            k: "" for k in fields
        }

class UpdateForm(UserChangeForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'avatar',
            'description',
            'province',
            'city'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['city'].queryset = City.objects.none()

        if 'province' in self.data:
            try:
                province_id = int(self.data.get('province'))
                self.fields['city'].queryset = City.objects.filter(province=province_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.province:
            self.fields['city'].queryset = self.instance.province.city_set.order_by('name')