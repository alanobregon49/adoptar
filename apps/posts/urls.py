from django.urls import path
from .views import feed

app_name = 'posts'
urlpatterns = [
    path('', feed, name="feed")
]
